const mongoose = require('mongoose');
const config = require('./config');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;
const collections = ['planets', 'areas', 'users'];

db.once('open', async () => {

  collections.forEach(async collectionName => {
    try {
      await db.dropCollection(collectionName);
    } catch (e) {
      console.log(`Collection ${collectionName} did not exist in DB`);
    }
  });

  const [Mars, Moon] = await Planet.create({
    name: 'Mars',
    earthType: true
  }, {
    name: 'Moon',
    earthType: true
  });

  await Area.create({

  })





}