const express = require('express');
const Planet = require('../models/Planet');

const router = express.Router();

const createRouter = (db) => {
  // Planet index
  router.get('/', (req, res) => {
    Planet.find()
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  // Planet get by ID
  router.get('/:id', (req, res) => {
    const id = req.params.id;
    db.collection('planets')
      .findOne({_id: new ObjectId(req.params.id)})
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });

  return router;
};

module.exports = createRouter;