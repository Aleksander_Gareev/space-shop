const express = require('express');
const Area = require('../models/Area');

const router = express.Router();

const createRouter = (db) => {
  router.get('/', (req, res)=> {
    Area.find()
      .then(results => res.send(results))
      .catch(() => res.send(500))
  });

  router.get('/:id', (req, res) => {
    const id = req.params.id;
    db.collection('areas')
      .findOne({_id: new ObjectId(req.params.id)})
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });

  return router;

};

module.exports = createRouter;