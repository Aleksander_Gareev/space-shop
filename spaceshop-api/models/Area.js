const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PlanetSchema = new Schema({
  quantity: {
    type: String,
    required: true
  },
  planet: {
    type: Schema.Types.ObjectId,
    ref: 'Planet',
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: true
  }
});

const Planet = mongoose.model('Planet', PlanetSchema);

module.exports = Planet;