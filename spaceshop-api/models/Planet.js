const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PlanetSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true
  },
  image: String

});

const Planet = mongoose.model('Planet', PlanetSchema);

module.exports = Planet;